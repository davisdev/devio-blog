<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PostRequestStatisticController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
    Route::get('/', [ArticleController::class, 'index'])->name('article.index');

    Route::prefix('article')->name('article.')->group(function () {
        Route::get('create', [ArticleController::class, 'create'])->name('create');
        Route::get('{article:slug}', [ArticleController::class, 'show'])->name('show');
        Route::get('{article:slug}/edit', [ArticleController::class, 'edit'])->name('edit');

        Route::post('/', [ArticleController::class, 'store'])->name('store');
        Route::post('{article:slug}', [ArticleController::class, 'update'])->name('update');
    });

    Route::get('intruders', [PostRequestStatisticController::class, 'index']);
    Route::get('intruders/clear', [PostRequestStatisticController::class, 'clear']);
});

require __DIR__ . '/auth.php';
