<?php

return [
    /**
     * Default user credentials for seeding.
     */
    'credentials' => [
        'name'     => 'Devio', // optional
        'email'    => 'demo@devio.com',
        'password' => 'demopassword123'
    ],

    /**
     * IP addresses that are excluded on POST request audit.
     */
    'whitelist_ips' => [
        '89.40.10.53', // local on server
        '127.0.0.1',
        'localhost',
        '0.0.0.0',
    ],
];
