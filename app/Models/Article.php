<?php

namespace App\Models;

use App\Http\Requests\AuthorizedRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Article extends Model implements HasMedia
{
    use HasFactory, HasSlug, InteractsWithMedia;

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'content',
        'is_published',
        'publishing_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'is_published' => 'boolean',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'publishing_at'
    ];

    /**
     * Eager load relationships.
     *
     * @var string[]
     */
    protected $with = ['user'];

    /**
     * Default pagination 'per_page' parameter value.
     *
     * @var int
     */
    protected $perPage = 5;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get model's REST-ful URL.
     *
     * @return string
     */
    public function url(): string
    {
        return url("article/{$this->slug}");
    }

    /**
     * Register 'image' collection for article.
     */
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('image')
            ->singleFile();
    }

    /**
     * Register image conversions for different scenarios.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('default')
            ->width(1200)
            ->height(400);

        $this->addMediaConversion('preview')
            ->width(800)
            ->height(250)
            ->quality(100);
    }

    /**
     * Show if article can be publicly viewed.
     *
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->is_published;
    }

    /**
     * @return bool
     */
    public function isScheduled(): bool
    {
        return ! $this->isPublic() && $this->publishing_at !== null;
    }

    /**
     * Scope only visible articles.
     *
     * @param $query
     * @return Builder
     */
    public function scopePublic($query): Builder
    {
        return $query->where('is_published', true);
    }

    /**
     * Scope yet to be published articles.
     *
     * @param $query
     * @return Builder
     */
    public function scopeUnpublished($query): Builder
    {
        return $query->where('publishing_at', '!=', null);
    }

    /**
     * Schedule article for scheduled posting.
     *
     * @param string $datetime
     * @return $this
     */
    public function schedule(string $datetime): self
    {
        return tap($this)->update([
            'publishing_at' => Carbon::createFromFormat('d-m-Y H:i', $datetime)
        ]);
    }

    /**
     * Determine if article should be scheduled for later posting.
     *
     * @param AuthorizedRequest $request
     * @return bool
     */
    public function schedulingNeeded(AuthorizedRequest $request): bool
    {
        return ! $request->getPublishingStatus()
            && $request->exists('scheduled_datetime')
            && $request->get('scheduled_datetime') !== null;
    }

    /**
     * @return $this
     */
    public function publish(): self
    {
        return tap($this)->update([
            'publishing_at' => null,
            'is_published'  => true
        ]);
    }
}
