<?php

namespace App\Http\Requests;

class UpdateArticleRequest extends AuthorizedRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'        => 'min:3|max:250',
            'content'      => 'min:15|max:2500',
            'image'        => 'image|max:5120',
            'is_published' => 'sometimes'
        ];
    }

    /**
     * Get "is_published" boolean value.
     *
     * @return bool
     */
    public function getPublishingStatus(): bool
    {
        return $this->exists('is_published')
            && $this->get('is_published') === "on";
    }
}
