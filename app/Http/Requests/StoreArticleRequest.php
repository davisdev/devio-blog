<?php

namespace App\Http\Requests;

use App\Rules\ValidFutureDateTime;

class StoreArticleRequest extends AuthorizedRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'              => 'required|min:3|max:250',
            'content'            => 'required|min:15|max:2500',
            'image'              => 'image|max:5120',
            'is_published'       => 'sometimes',
            'scheduled_datetime' => ['exclude_if:is_published,on', new ValidFutureDateTime($this->get('is_published'))]
        ];
    }

    /**
     * Get "is_published" boolean value.
     *
     * @return bool
     */
    public function getPublishingStatus(): bool
    {
        return $this->exists('is_published')
            && $this->get('is_published') === "on";
    }
}
