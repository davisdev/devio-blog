<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class ArticleController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('dashboard', [
            'articles' => Article::latest()->paginate(),
        ]);
    }

    /**
     * @param Article $article
     * @return View
     */
    public function show(Article $article): View
    {
        return view('articles.show', compact('article'));
    }

    /**
     * @param Article $article
     * @return View
     * @throws AuthorizationException
     */
    public function edit(Article $article): View
    {
        $this->authorize('update', $article);

        return view('articles.edit', compact('article'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('articles.create');
    }

    /**
     * @param StoreArticleRequest $request
     * @return RedirectResponse
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function store(StoreArticleRequest $request): RedirectResponse
    {
        /** @var Article $article */
        $article = auth()->user()->articles()->create(
            array_merge($request->validated(), ['is_published' => $request->getPublishingStatus()])
        );

        if ($request->has('image')) {
            $article->addMediaFromRequest('image')
                ->toMediaCollection('image');
        }

        $message = "Article has been created!";


        if ($article->schedulingNeeded($request)) {
            $article->schedule($request->get('scheduled_datetime'));

            $message .= " It will be published {$article->publishing_at->diffForHumans()}";
        }

        session()->flash('success', $message);

        return redirect($article->url());
    }

    /**
     * @param UpdateArticleRequest $request
     * @param Article $article
     * @return RedirectResponse
     * @throws AuthorizationException
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function update(UpdateArticleRequest $request, Article $article): RedirectResponse
    {
        $this->authorize('update', $article);

        $article->update(
            array_merge($request->validated(), ['is_published' => $request->getPublishingStatus()])
        );

        if ($article->isDirty('is_published') && $request->getPublishingStatus()) {
            // If article was scheduled and after update you manually publish it,
            // schedule should be cleaned from article.
            $article->update(['publishing_at' => null]);
        }

        if ($request->has('image')) {
            $article->addMediaFromRequest('image')
                ->toMediaCollection('image');
        }

        session()->flash('success', 'Article has been updated!');

        return redirect($article->url());
    }
}
