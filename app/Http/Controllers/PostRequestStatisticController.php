<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostRequestResourceCollection;
use App\Jobs\ClearIntruderList;
use App\Models\PostRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class PostRequestStatisticController extends Controller
{
    public function index(): JsonResource
    {
        return PostRequestResourceCollection::make(
            PostRequest::latest()->paginate(25)
        );
    }

    public function clear(): JsonResponse
    {
        ClearIntruderList::dispatch();

        return response()->json([
            'success' => true,
            'message' => 'Intruder list cleared!',
        ]);
    }
}
