<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostRequestResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'ip_address'   => $this->ip_address === '127.0.0.1' ? 'localhost' : $this->ip_address,
            'user_agent'   => $this->user_agent,
            'path'         => $this->path,
            'payload'      => json_decode($this->payload, true),
            'requested_at' => $this->created_at->format('H:i:s, d.m.Y'),
        ];
    }
}
