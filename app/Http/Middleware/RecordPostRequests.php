<?php

namespace App\Http\Middleware;

use App\Models\PostRequest;
use Closure;
use Illuminate\Http\Request;

class RecordPostRequests
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->shouldRecord($request)) {
            PostRequest::create([
                'ip_address' => $request->getClientIp(),
                'user_agent' => $request->userAgent(),
                'path'       => $request->path(),
                'payload'    => json_encode($request->all()),
            ]);
        }

        return $next($request);
    }

    public function shouldRecord(Request $request): bool
    {
        return $request->isMethod('post')
            && $this->isForeignClient($request->getClientIp());
    }

    private function isForeignClient(string $ipv4): bool
    {
        return ! in_array($ipv4, $this->whitelistedIps());
    }

    private function whitelistedIps(): array
    {
        return config('project.whitelist_ips');
    }
}
