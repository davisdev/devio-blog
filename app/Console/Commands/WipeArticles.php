<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;

class WipeArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:wipe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wipe articles table (along with images associated)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Article::truncate();

        $this->call("media-library:clear");

        $this->info('Articles table has been cleaned. Oh, and article images are goners as well...');

        return 0;
    }
}
