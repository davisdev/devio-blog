<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ValidFutureDateTime implements Rule
{
    /**
     * @var bool
     */
    private bool $shouldPass;

    /**
     * ValidFutureDateTime constructor.
     * @param mixed $isPublished Manual publish switch status
     */
    public function __construct(mixed $isPublished)
    {
        $this->shouldPass = $isPublished === null;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->shouldPass ||
            ! is_null($value)
            && Carbon::parse($value)->isFuture();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not in the future.';
    }
}
