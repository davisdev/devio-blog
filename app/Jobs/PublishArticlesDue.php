<?php

namespace App\Jobs;

use App\Models\Article;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class PublishArticlesDue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Article::unpublished()
            ->chunkById(50, function (Collection $articles) {
                $articles->each(function (Article $article) {
                    if ($article->publishing_at->gte(now())) {
                        // Articles that are not due for now,
                        // will be left un-published.
                        return;
                    }

                    $article->publish();
                });
        });
    }
}
