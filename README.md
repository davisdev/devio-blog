# About project
This is a simple blog where you can post with or without scheduled posting for each article. Uploading an image is allowed for each post.

# Features
* Scheduled posting to specific date and time (for being reasonable, max 30 days in future)
* Ability to add appropriate image (which will be resized on back-end)
* Authentication (but credentials are provided to get to the point quicker)
* Ability to adjust credentials (in config file `<projectroot>/configs/project.php`)
* Post pagination (you'll see it after you have more than 5 posts)
* Automatically regenerated unique & readable slug-url generation for each post (both after storing/updating)
* Manual publishing "switch" if article should be available for public eye immediately
* Only you can edit your own posts

# Installation
1. Clone the project through HTTPS/SSH
2. Copy example environment file `cp .env.example`
3. Install both PHP and JS dependencies `composer update && npm update`
4. Compile JS `npm run dev`
5. Create a database locally and add credentials in `.env` file
6. Run `php artisan migrate`
7. Run `php artisan serve` to "run" the site. Now you can visit `http://localhost:8000` and go from there
8. If you are too lazy to register, run `php artisan db:seed`. First time it will give you credentials to log in with and seed some articles for you.

# NB
* You *will* have to run scheduler through your crontab. Add this line
`* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`
  in your crontab (will run every minute). Without this you'll have to manually loop the `php artisan schedule:run` command to make scheduled posting work.

# Out of scope for the test:
1. Unit/feature tests (just not enough time to test all with good coverage)
2. Some sort of "cabinet" where user could see posts scheduled
3. Ability to switch up schedule to later/earlier on (could've done it, but seems a bit too out of scope)
4. Caching article index page. For efficient caching we'd require setting up Redis, but that would only be more feasible in dockerized environment
