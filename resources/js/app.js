require('./bootstrap');

require('alpinejs');


import flatpickr from "flatpickr";

flatpickr('#datetime', {
    enableTime: true,
    dateFormat: "d-m-Y H:i",
    minDate: new Date().getTime(),
    maxDate: new Date().fp_incr(30)
});

let datepickerNode = document.querySelector('#datetime_block')

if (datepickerNode) {
    let manualPublishSwitch = document.querySelector('#is_published')

    manualPublishSwitch.addEventListener(
        'change',
        ({target}) => {
            if (target.checked) {
                datepickerNode.classList.add('hidden')
                datepickerNode.disabled = true
            } else {
                datepickerNode.classList.remove('hidden')
                datepickerNode.disabled = false
            }
        }
    )
}

