@if($errors->any())
    @foreach ($errors->all() as $error)
        <div class="py-3 px-5 mb-5 rounded-lg text-gray-100 bg-red-500">
            {{ $error }}
        </div>
    @endforeach
@endif

@if (session()->has('success'))
    <div class="py-3 px-5 mb-5 rounded-lg text-gray-100 bg-green-500">
        {{ session()->get('success') }}
    </div>
@endif
