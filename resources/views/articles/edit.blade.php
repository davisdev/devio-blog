<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a class="block mb-5 text-blue-700" href="{{ route('article.index') }}">&laquo; Back to article</a>

                    @include('messages')

                    <form action="{{ route('article.update', $article) }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <article>
                            <div class="article-header flex flex-col mb-4">
                                @if ($image = $article->getFirstMedia('image'))
                                    <img class="h-48 w-full mb-5 object-contain" src="{{ $image->getUrl('preview') }}" alt="{{ $article->title }}">
                                @endif
                                <label for="image">Article image <span class="text-sm">(recommended width: 1200px or less)</span></label>
                                <input type="file" id="image" name="image" accept="image/jpeg,image/png">
                            </div>

                            <div class="article-header flex flex-col mb-4">
                                <label for="title">Article title <span class="text-red-500">*</span></label>
                                <input type="text" name="title" id="title" class="w-full" value="{{ old('title', $article->title) }}">
                            </div>

                            <div class="article-content mb-4">
                                <label for="content">Article content <span class="text-red-500">*</span></label>
                                <textarea name="content" id="content" rows="10" class="w-full">{{ old('content', $article->content) }}</textarea>
                            </div>

                            <div class="additional mb-4">
                                <label for="is_published" class="pr-2">Is this article publicly available?</label>
                                <input type="checkbox" name="is_published" id="is_published" {{ $article->is_published ? 'checked' : '' }}>
                            </div>

                            <div class="legend">
                                <p><span class="text-red-500">*</span> Mandatory fields</p>
                            </div>
                        </article>
                        <div class="actions w-full flex justify-end">
                            <a href="{{ route('article.index') }}" onclick="return confirm('Are you sure? Changes made will be lost.')" class="rounded-lg text-white py-2 px-4 bg-red-500 hover:bg-red-600 focus:outline-none">Cancel</a>
                            <button type="submit" class="rounded-lg text-white ml-5 py-2 px-4 bg-blue-500 hover:bg-blue-600 focus:outline-none">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
