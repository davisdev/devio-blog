<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a class="block mb-5 text-blue-700" href="{{ route('article.index') }}">&laquo;  Back to article list</a>

                    @include('messages')

                    <article>
                        @if ($image = $article->getFirstMedia('image'))
                            <img class="mb-5 object-cover h-96 w-full" src="{{ $image->getUrl('default') }}" alt="{{ $article->title }}">
                        @endif

                        <div class="article-header flex justify-between mb-4">
                            <h1 class="text-xl font-bold">{{ $article->title }}</h1>
                            <p class="flex">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                </svg>
                                @if (! $article->isScheduled())
                                    {{ $article->created_at->format('F j, Y') }}
                                @else
                                    Publishing: {{ $article->publishing_at->diffForHumans() }}
                                @endif
                            </p>
                        </div>
                        <div class="article-content mb-4">
                            {{ $article->content }}
                        </div>
                        <div class="article-footer flex justify-end">
                            <div class="actions w-full flex justify-between align-middle">
                                @can('update', $article)
                                    <a href="{{ route('article.edit', ['article' => $article]) }}" class="flex rounded-lg text-white py-2 px-4 bg-blue-500 hover:bg-blue-600 focus:outline-non">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                        </svg>
                                        Edit
                                    </a>
                                @endcan
                                <span>By: {{ $article->user->name }}</span>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
