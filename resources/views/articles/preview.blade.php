<article class="p-4 mb-3 border-b-2">
    <div class="article-header flex flex-col justify-between mb-4">
        @if ($image = $article->getFirstMedia('image'))
            <img class="h-48 w-full mb-5 object-cover" src="{{ $image->getUrl('preview') }}" alt="{{ $article->title }}">
        @endif
        <div>
            <a href="{{ $article->url() }}" class="text-xl font-bold">{{ $article->title }}</a>
            @if (! $article->isPublic())
                <span class="p-1 text-xs text-gray-100 bg-red-400 rounded-md">Not public</span>
            @endif
        </div>
        @if (! $article->isScheduled())
            <p>{{ $article->created_at->format('F j, Y') }}</p>
        @else
            <p>Publishing: {{ $article->publishing_at->diffForHumans() }}</p>
        @endif
    </div>
    <div class="article-content mb-2">{{ \Illuminate\Support\Str::words($article->content, 25) }}</div>
    <div class="article-footer flex justify-end">
        <div class="actions w-1/4 flex justify-between">
            @can('update', $article)
                <a class="hover:underline flex" href="{{ $article->url() . '/edit' }}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                    </svg>
                    Edit article
                </a>
            @endcan
            <a class="pl-4 hover:underline flex" href="{{ $article->url() }}">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z" />
                </svg>
                Read further
            </a>
        </div>
    </div>
</article>
