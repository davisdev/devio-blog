<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse($articles as $article)
                        @include('articles.preview', $article)
                    @empty
                        There are no articles available. Do you want to <a class="font-bold text-blue-500" href="{{ route('article.create') }}">create a new one</a>?
                    @endforelse

                    {{ $articles->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
