<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DatabaseSeeder extends Seeder
{
    /**
     * @var array
     */
    private array $config;

    /**
     * DatabaseSeeder constructor.
     */
    public function __construct()
    {
        $this->config = config('project.credentials');
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Not much seeding is needed, let's just keep it here instead.
        $user = $this->createTestUser();

        $this->seedArticlesFor($user, 15);
    }

    /**
     * @return User
     */
    private function createTestUser(): User
    {
        /** @var User $user */
        $user = User::firstOrCreate(
            ['email' => $email = Arr::get($this->config, 'email', 'demo@devio-blog.com')],
            [
                'name'     => Arr::get($this->config, 'name', 'Developer'),
                'password' => bcrypt(
                    $password = Arr::get($this->config, 'password', 'demopassword123')
                )
            ]
        );

        $this->command->info('Test user created!');

        if ($user->wasRecentlyCreated) {
            $this->command->table(['E-mail', 'Password'], [
                [$email, $password]
            ]);
        }

        return $user;
    }

    /**
     * @param User $user
     * @param int $amount Amount of articles to seed for user
     */
    private function seedArticlesFor(User $user, int $amount = 20): void
    {
        if ($user->articles()->count()) {
            $this->command->warn(
                'No new articles have been seeded! In order to seed fresh articles, run `php artisan articles:wipe` and then `php artisan db:seed`.'
            );

            return;
        }

        Article::factory()->count($amount)->create([
            'user_id' => $user->id
        ]);

        $this->command->info('New articles have been seeded!');
    }
}
