<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'       => User::factory(),
            'title'         => $this->faker->sentence(),
            'content'       => $this->faker->paragraph(),
            'is_published'  => $isPublishedManually = ! ! random_int(0, 1),
            'publishing_at' => $isPublishedManually
                ? null
                : now()->addDays(random_int(1, 20))
        ];
    }
}
